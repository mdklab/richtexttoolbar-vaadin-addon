package org.vaadin.addons.richtexttoolbar.demo;

import org.vaadin.addons.richtexttoolbar.RichTextArea;
import org.vaadin.addons.richtexttoolbar.RichTextToolbar;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("RichTextToolbar Add-on Demo")
@Widgetset("org.vaadin.addons.richtexttoolbar.RichTextToolbarWidgetset")
@SuppressWarnings("serial")
@Theme("richtexttoolbardemo")
public class DemoUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);
        final RichTextToolbar rtt = new RichTextToolbar();
        final RichTextArea rta = new RichTextArea();
        rta.setImmediate(true);
        layout.addComponent(rtt);
        final HorizontalLayout hl1 = new HorizontalLayout();
        hl1.setMargin(true);
        final HorizontalLayout hl2 = new HorizontalLayout();
        hl2.setMargin(true);
        hl1.addComponent(rta);
        layout.addComponent(hl1);
        layout.addComponent(hl2);
        Button b = new Button("Toggle single line", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                rtt.setSingleLinePanel(!rtt.isSingleLinePanel());
            }
        });
        layout.addComponent(b);
        b = new Button("Toggle toolbar connection", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                if (rta.getToolbar() == null)
                    rta.setToolbar(rtt);
                else
                    rta.setToolbar(null);
            }
        });
        layout.addComponent(b);

        b = new Button("Toggle autoGrowHeight", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                rta.setAutoGrowHeight(!rta.isAutoGrowHeight());
                // rta.setAutoGrowWidth(!rta.isAutoGrowWidth());
            }
        });
        layout.addComponent(b);
        b = new Button("Toggle layout", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                if (hl1.getComponentCount() == 0)
                    hl1.moveComponentsFrom(hl2);
                else
                    hl2.moveComponentsFrom(hl1);
            }
        });
        layout.addComponent(b);
        b = new Button("Toggle Readonly", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                rta.setReadOnly(!rta.isReadOnly());
            }
        });
        layout.addComponent(b);
        b = new Button("show value", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                Notification.show(rta.getValue());
            }
        });
        layout.addComponent(b);
        b = new Button("set  a value", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                rta.setValue("Foo<br/>Bar");
            }
        });
        layout.addComponent(b);
    }

}
