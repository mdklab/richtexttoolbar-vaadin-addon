package org.vaadin.addons.richtexttoolbar;

import org.vaadin.addons.richtexttoolbar.client.richtexttoolbar.RichTextToolbarState;

// This is the server-side UI component that provides public API 
// for RichTextToolbar
public class RichTextToolbar extends com.vaadin.ui.AbstractComponent {

    public RichTextToolbar() {
    }

    @Override
    public RichTextToolbarState getState() {
        return (RichTextToolbarState) super.getState();
    }

    boolean singleLinePanel = false;

    public boolean isSingleLinePanel() {
        return singleLinePanel;
    }

    public void setSingleLinePanel(boolean singleLinePanel) {
        this.singleLinePanel = singleLinePanel;
        getState().singleLinePanel = this.singleLinePanel;
    }
}
