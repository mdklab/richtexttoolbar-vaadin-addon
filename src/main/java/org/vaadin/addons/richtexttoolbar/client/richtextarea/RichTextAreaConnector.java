/*
 * Copyright 2000-2013 Vaadin Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.vaadin.addons.richtexttoolbar.client.richtextarea;

import org.vaadin.addons.richtexttoolbar.RichTextArea;
import org.vaadin.addons.richtexttoolbar.client.VRichTextArea;
import org.vaadin.addons.richtexttoolbar.client.VRichTextToolbar;
import org.vaadin.addons.richtexttoolbar.client.richtexttoolbar.RichTextToolbarConnector;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.user.client.Event;
import com.vaadin.client.ApplicationConnection;
import com.vaadin.client.Paintable;
import com.vaadin.client.UIDL;
import com.vaadin.client.ui.AbstractFieldConnector;
import com.vaadin.client.ui.ShortcutActionHandler.BeforeShortcutActionListener;
import com.vaadin.client.ui.SimpleManagedLayout;
import com.vaadin.shared.ui.Connect;
import com.vaadin.shared.ui.Connect.LoadStyle;
import com.vaadin.shared.util.SharedUtil;

@Connect(value = RichTextArea.class, loadStyle = LoadStyle.LAZY)
public class RichTextAreaConnector extends AbstractFieldConnector implements
        Paintable, BeforeShortcutActionListener, SimpleManagedLayout {

    /*
     * Last value received from the server
     */
    private String cachedValue = "";

    @Override
    protected void init() {
        getWidget().addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                flush();
            }
        });
        if (getWidget().formatter != null)
            getLayoutManager().registerDependency(this,
                    getWidget().formatter.getElement());
    }

    @Override
    public void onUnregister() {
        super.onUnregister();
        if (getWidget().formatter != null)
            getLayoutManager().unregisterDependency(this,
                    getWidget().formatter.getElement());
    }

    @Override
    public void updateFromUIDL(final UIDL uidl, ApplicationConnection client) {
        getWidget().client = client;
        getWidget().id = uidl.getId();

        if (uidl.hasAttribute("internalFormatter")) {
            boolean intF = uidl.getBooleanAttribute("internalFormatter");
            getWidget().setInternalFormatter(intF);
            if (intF)
                getLayoutManager().registerDependency(this,
                        getWidget().formatter.getElement());
            else
                getLayoutManager().unregisterDependency(this,
                        getWidget().formatter.getElement());
        }
        if (uidl.hasAttribute("toolbar")) {
            RichTextToolbarConnector rttc = (RichTextToolbarConnector) uidl
                    .getPaintableAttribute("toolbar", client);
            getWidget().setFormatter(rttc.getWidget());
        }

        int newMaxLength = uidl.hasAttribute("maxLength") ? uidl
                .getIntAttribute("maxLength") : -1;
        boolean autoGrowWidth = uidl.hasAttribute("autoGrowWidth") ? uidl
                .getBooleanAttribute("autoGrowWidth") : false;
        boolean autoGrowHeight = uidl.hasAttribute("autoGrowHeight") ? uidl
                .getBooleanAttribute("autoGrowHeight") : false;
        if (newMaxLength >= 0 || autoGrowHeight || autoGrowWidth) {
            if (getWidget().keyPressHandler == null) {
                getWidget().keyPressHandler = getWidget().rta
                        .addKeyPressHandler(getWidget());
            }
            getWidget().maxLength = newMaxLength;
        } else if (getWidget().keyPressHandler != null) {
            getWidget().getElement().setAttribute("maxlength", "");
            getWidget().keyPressHandler.removeHandler();
            getWidget().keyPressHandler = null;
        }
        getWidget().maxLength = newMaxLength;
        getWidget().autoGrowWidth = autoGrowWidth;
        getWidget().autoGrowHeight = autoGrowHeight;

        if (uidl.hasAttribute("selectAll")) {
            getWidget().selectAll();
        }

        if (uidl.hasVariable("text")) {
            String newValue = uidl.getStringVariable("text");
            if (!SharedUtil.equals(newValue, cachedValue)) {
                getWidget().setValue(newValue);
                cachedValue = newValue;
            }
        }
        if (!isRealUpdate(uidl)) {
            return;
        }
        getWidget().setEnabled(isEnabled());
        getWidget().setReadOnly(isReadOnly());
        getWidget().immediate = getState().immediate;

    }

    @Override
    public void onBeforeShortcutAction(Event e) {
        flush();
    }

    @Override
    public VRichTextArea getWidget() {
        return (VRichTextArea) super.getWidget();
    }

    @Override
    public void flush() {
        if (getConnection() != null && getConnectorId() != null) {
            final String html = getWidget().getSanitizedValue();
            if (!html.equals(cachedValue)) {
                cachedValue = html;
                getConnection().updateVariable(getConnectorId(), "text", html,
                        getState().immediate);
            }
        }
    };

    @Override
    public void layout() {
        if (!isUndefinedHeight()) {
            int rootElementInnerHeight = getLayoutManager().getInnerHeight(
                    getWidget().getElement());
            final VRichTextToolbar formatter = getWidget().formatter;
            int formatterHeight = (getWidget().internalFormatter && formatter != null) ? getLayoutManager()
                    .getOuterHeight(formatter.getElement()) : 0;
            int editorHeight = rootElementInnerHeight - formatterHeight;
            if (editorHeight < 0) {
                editorHeight = 0;
            }
            getWidget().rta.setHeight(editorHeight + "px");
        }
    }

}
