package org.vaadin.addons.richtexttoolbar.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.OptionElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RichTextArea.Formatter;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.vaadin.client.ApplicationConnection;

// Extend any GWT Widget
public class VRichTextToolbar extends Composite {

    /**
     * We use an inner EventHandler class to avoid exposing event methods on the RichTextToolbar itself.
     */
    private class EventHandler implements ClickHandler, ChangeHandler, KeyUpHandler {

        @Override
        public void onChange(ChangeEvent event) {
            Object sender = event.getSource();
            if (currentRTA == null)
                return;
            Formatter formatter = currentRTA.getRTAFormatter();
            if (sender == backColors) {
                formatter.setBackColor(backColors.getValue(backColors.getSelectedIndex()));
                backColors.setSelectedIndex(0);
            } else if (sender == foreColors) {
                formatter.setForeColor(foreColors.getValue(foreColors.getSelectedIndex()));
                foreColors.setSelectedIndex(0);
            } else if (sender == fonts) {
                formatter.setFontName(fonts.getValue(fonts.getSelectedIndex()));
                fonts.setSelectedIndex(0);
            } else if (sender == fontSizes) {
                formatter.setFontSize(fontSizesConstants[fontSizes.getSelectedIndex() - 1]);
                fontSizes.setSelectedIndex(0);
            }
        }

        @Override
        public void onClick(ClickEvent event) {
            Object sender = event.getSource();
            if (currentRTA == null)
                return;
            Formatter formatter = currentRTA.getRTAFormatter();
            if (sender == bold) {
                formatter.toggleBold();
            } else if (sender == italic) {
                formatter.toggleItalic();
            } else if (sender == underline) {
                formatter.toggleUnderline();
            } else if (sender == subscript) {
                formatter.toggleSubscript();
            } else if (sender == superscript) {
                formatter.toggleSuperscript();
            } else if (sender == strikethrough) {
                formatter.toggleStrikethrough();
            } else if (sender == indent) {
                formatter.rightIndent();
            } else if (sender == outdent) {
                formatter.leftIndent();
            } else if (sender == justifyLeft) {
                formatter.setJustification(RichTextArea.Justification.LEFT);
            } else if (sender == justifyCenter) {
                formatter.setJustification(RichTextArea.Justification.CENTER);
            } else if (sender == justifyRight) {
                formatter.setJustification(RichTextArea.Justification.RIGHT);
            } else if (sender == insertImage) {
                final String url = Window.prompt("Enter an image URL:", "http://");
                if (url != null) {
                    formatter.insertImage(url);
                }
            } else if (sender == createLink) {
                final String url = Window.prompt("Enter a link URL:", "http://");
                if (url != null) {
                    formatter.createLink(url);
                }
            } else if (sender == removeLink) {
                formatter.removeLink();
            } else if (sender == hr) {
                formatter.insertHorizontalRule();
            } else if (sender == ol) {
                formatter.insertOrderedList();
            } else if (sender == ul) {
                formatter.insertUnorderedList();
            } else if (sender == removeFormat) {
                formatter.removeFormat();
            } else if (sender == currentRTA.getRTA()) {
                // We use the RichTextArea's onKeyUp event to update the
                // toolbar
                // status. This will catch any cases where the user moves the
                // cursur using the keyboard, or uses one of the browser's
                // built-in keyboard shortcuts.
                updateStatus();
            } else {
                for (VRichTextArea rta : attachedRTAs) {
                    if (sender == rta.getRTA()) {
                        currentRTA = rta;
                        updateStatus();
                    }
                }
            }
        }

        @Override
        public void onKeyUp(KeyUpEvent event) {
            if (currentRTA == null)
                return;
            if (event.getSource() == currentRTA.getRTA()) {
                // We use the RichTextArea's onKeyUp event to update the toolbar
                // status. This will catch any cases where the user moves the
                // cursor using the keyboard, or uses one of the browser's
                // built-in keyboard shortcuts.
                updateStatus();
            }
        }
    }

    /**
     * The input node CSS classname.
     */
    public static final String CLASSNAME = "v-richtexttoolbar";
    /** For internal use only. May be removed or replaced in the future. */
    public String id;

    /** For internal use only. May be removed or replaced in the future. */
    public ApplicationConnection client;

    private static final RichTextArea.FontSize[] fontSizesConstants = new RichTextArea.FontSize[] {
            RichTextArea.FontSize.XX_SMALL, RichTextArea.FontSize.X_SMALL, RichTextArea.FontSize.SMALL,
            RichTextArea.FontSize.MEDIUM, RichTextArea.FontSize.LARGE, RichTextArea.FontSize.X_LARGE,
            RichTextArea.FontSize.XX_LARGE };

    private final EventHandler handler = new EventHandler();

    private VRichTextArea currentRTA;
    public final List<VRichTextArea> attachedRTAs = new ArrayList<VRichTextArea>();
    private final Map<VRichTextArea, List<HandlerRegistration>> rtaHandlers = new HashMap<VRichTextArea, List<HandlerRegistration>>();

    private boolean singleLinePanel = false;

    private final FlowPanel panel = new FlowPanel();
    private ToggleButton bold;
    private ToggleButton italic;
    private ToggleButton underline;
    private ToggleButton subscript;
    private ToggleButton superscript;
    private ToggleButton strikethrough;
    private PushButton indent;
    private PushButton outdent;
    private PushButton justifyLeft;
    private PushButton justifyCenter;
    private PushButton justifyRight;
    private PushButton hr;
    private PushButton ol;
    private PushButton ul;
    private PushButton insertImage;
    private PushButton createLink;
    private PushButton removeLink;
    private PushButton removeFormat;

    private ListBox backColors;
    private ListBox foreColors;
    private ListBox fonts;
    private ListBox fontSizes;

    private SimplePanel spacer;

    /**
     * Creates a new toolbar that drives the given rich text area.
     * 
     * @param richText
     *            the rich text area to be controlled
     */
    public VRichTextToolbar() {

        initWidget(panel);
        setStyleName(CLASSNAME);

        panel.add(bold = createToggleButton("rtt-b"));
        panel.add(italic = createToggleButton("rtt-i"));
        panel.add(underline = createToggleButton("rtt-u"));
        panel.add(subscript = createToggleButton("rtt-sub"));
        panel.add(superscript = createToggleButton("rtt-super"));
        panel.add(justifyLeft = createPushButton("rtt-left"));
        panel.add(justifyCenter = createPushButton("rtt-center"));
        panel.add(justifyRight = createPushButton("rtt-right"));
        panel.add(strikethrough = createToggleButton("rtt-strike"));
        panel.add(indent = createPushButton("rtt-indent"));
        panel.add(outdent = createPushButton("rtt-outdent"));
        panel.add(hr = createPushButton("rtt-hr"));
        panel.add(ol = createPushButton("rtt-ol"));
        panel.add(ul = createPushButton("rtt-ul"));
        panel.add(insertImage = createPushButton("rtt-img"));
        panel.add(createLink = createPushButton("rtt-link"));
        panel.add(removeLink = createPushButton("rtt-unlink"));
        panel.add(removeFormat = createPushButton("rtt-clear"));
        spacer = new SimplePanel();
        spacer.getElement().addClassName("rtt-spacer");
        panel.add(spacer);

        SimplePanel div = new SimplePanel(fonts);
        backColors = createColorList(Character.toString((char) 0xf14b));
        backColors.getElement().addClassName("rtt-bgselect");
        div = new SimplePanel(backColors);
        div.getElement().addClassName("rtt-bgcolor");
        panel.add(div);

        foreColors = createColorList(Character.toString((char) 0xf040));
        foreColors.getElement().addClassName("rtt-fselect");
        div = new SimplePanel(foreColors);
        div.getElement().addClassName("rtt-fcolor");
        panel.add(div);

        fonts = createFontList();
        fonts.getElement().addClassName("rtt-familyselect");
        div = new SimplePanel(fonts);
        div.getElement().addClassName("rtt-family");
        panel.add(div);

        fontSizes = createFontSizes();
        fontSizes.getElement().addClassName("rtt-sizeselect");
        div = new SimplePanel(fontSizes);
        div.getElement().addClassName("rtt-size");
        panel.add(div);
    }

    private ListBox createColorList(String text) {
        final ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);
        lb.addItem(text, "");
        lb.addItem("", "white");
        lb.addItem("", "black");
        lb.addItem("", "red");
        lb.addItem("", "green");
        lb.addItem("", "yellow");
        lb.addItem("", "blue");
        NodeList<Node> children = lb.getElement().getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            OptionElement o = children.getItem(i).cast();
            o.getStyle().setBackgroundColor(o.getValue());
        }
        lb.setTabIndex(-1);
        return lb;
    }

    private ListBox createFontList() {
        final ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);

        lb.addItem(Character.toString((char) 0xf031), "");
        lb.addItem("Times New Roman", "Times New Roman");
        lb.addItem("Arial", "Arial");
        lb.addItem("Courier New", "Courier New");
        lb.addItem("Georgia", "Georgia");
        lb.addItem("Trebuchet", "Trebuchet");
        lb.addItem("Verdana", "Verdana");
        lb.setTabIndex(-1);
        return lb;
    }

    private ListBox createFontSizes() {
        final ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);

        lb.addItem(Character.toString((char) 0xf034));
        lb.addItem("XS");
        lb.addItem("S");
        lb.addItem("M");
        lb.addItem("L");
        lb.addItem("XL");
        lb.addItem("XXL");
        lb.setTabIndex(-1);
        return lb;
    }

    private PushButton createPushButton(String className) {
        final PushButton pb = new PushButton();
        pb.getElement().addClassName(className);
        pb.addClickHandler(handler);
        pb.setTabIndex(-1);
        return pb;
    }

    private ToggleButton createToggleButton(String className) {
        final ToggleButton tb = new ToggleButton();
        tb.getElement().addClassName(className);
        tb.addClickHandler(handler);
        tb.setTabIndex(-1);
        return tb;
    }

    /**
     * Updates the status of all the stateful buttons.
     */
    private void updateStatus() {
        if (currentRTA == null)
            return;
        Formatter formatter = currentRTA.getRTAFormatter();
        bold.setDown(formatter.isBold());
        italic.setDown(formatter.isItalic());
        underline.setDown(formatter.isUnderlined());
        subscript.setDown(formatter.isSubscript());
        superscript.setDown(formatter.isSuperscript());
        strikethrough.setDown(formatter.isStrikethrough());
    }

    public void setSingleLinePanel(boolean singleLinePanel) {
        if (this.singleLinePanel == singleLinePanel)
            return;
        else if (singleLinePanel) {
            spacer.setVisible(false);
        } else {
            spacer.setVisible(true);
        }
        this.singleLinePanel = singleLinePanel;
    }

    public void attachVRTA(VRichTextArea vrta) {
        if (!attachedRTAs.contains(vrta)) {
            attachedRTAs.add(vrta);
            List<HandlerRegistration> handlerregs = new ArrayList<HandlerRegistration>(2);
            handlerregs.add((vrta).getRTA().addKeyUpHandler(handler));
            handlerregs.add((vrta).getRTA().addClickHandler(handler));
            rtaHandlers.put(vrta, handlerregs);
            if (currentRTA == null)
                currentRTA = attachedRTAs.get(0);
        }
    }

    public void detachVRTA(VRichTextArea vrta) {
        if (vrta == null || !attachedRTAs.contains(vrta))
            return;
        for (HandlerRegistration hr : rtaHandlers.get(vrta))
            if (hr != null)
                hr.removeHandler();
        attachedRTAs.remove(vrta);
        if (currentRTA != null && currentRTA.equals(vrta))
            if (attachedRTAs.size() == 0)
                currentRTA = null;
            else
                currentRTA = attachedRTAs.get(0);
    }

}