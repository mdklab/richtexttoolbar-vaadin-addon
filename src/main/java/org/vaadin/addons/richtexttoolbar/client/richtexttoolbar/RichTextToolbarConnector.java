package org.vaadin.addons.richtexttoolbar.client.richtexttoolbar;

import org.vaadin.addons.richtexttoolbar.RichTextToolbar;
import org.vaadin.addons.richtexttoolbar.client.VRichTextToolbar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.shared.ui.Connect;

@Connect(RichTextToolbar.class)
public class RichTextToolbarConnector extends AbstractComponentConnector {

    public RichTextToolbarConnector() {
    }

    @Override
    protected Widget createWidget() {
        return GWT.create(VRichTextToolbar.class);
    }

    @Override
    public VRichTextToolbar getWidget() {
        return (VRichTextToolbar) super.getWidget();
    }

    @Override
    public RichTextToolbarState getState() {
        return (RichTextToolbarState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        final boolean singleLine = getState().singleLinePanel;
        getWidget().setSingleLinePanel(singleLine);
    }
}
