/*
 * Copyright 2000-2013 Vaadin Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.vaadin.addons.richtexttoolbar.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.BodyElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.InitializeEvent;
import com.google.gwt.event.logical.shared.InitializeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RichTextArea.Formatter;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.ApplicationConnection;
import com.vaadin.client.BrowserInfo;
import com.vaadin.client.ConnectorMap;
import com.vaadin.client.ui.Field;
import com.vaadin.client.ui.ShortcutActionHandler;
import com.vaadin.client.ui.ShortcutActionHandler.ShortcutActionHandlerOwner;
import com.vaadin.client.ui.TouchScrollDelegate;

/**
 * This class implements a basic client side rich text editor component.
 * 
 * @author Vaadin Ltd.
 * 
 */
public class VRichTextArea extends Composite implements Field, KeyPressHandler,
        KeyDownHandler, Focusable {

    /**
     * The input node CSS classname.
     */
    public static final String CLASSNAME = "v-richtextarea";

    /** For internal use only. May be removed or replaced in the future. */
    public String id;

    /** For internal use only. May be removed or replaced in the future. */
    public ApplicationConnection client;

    /** For internal use only. May be removed or replaced in the future. */
    public boolean immediate = false;

    /** For internal use only. May be removed or replaced in the future. */
    public RichTextArea rta;

    /** For internal use only. May be removed or replaced in the future. */
    public VRichTextToolbar formatter;

    /** For internal use only. May be removed or replaced in the future. */
    public boolean internalFormatter = false;

    /** For internal use only. May be removed or replaced in the future. */
    public HTML html = new HTML();

    private final FlowPanel fp = new FlowPanel();

    private boolean enabled = true;

    /** For internal use only. May be removed or replaced in the future. */
    public int maxLength = -1;

    private int toolbarNaturalWidth = 500;

    /** For internal use only. May be removed or replaced in the future. */
    public HandlerRegistration keyPressHandler;

    private ShortcutActionHandlerOwner hasShortcutActionHandler;

    private boolean readOnly = false;

    public boolean autoGrowWidth;
    public boolean autoGrowHeight;

    private boolean initialised = false;

    private int lastAutoHeight = 0;
    private int lastNewLineCount = Integer.MAX_VALUE;
    private int lineToHeightFactor = 0;

    private final Map<BlurHandler, HandlerRegistration> blurHandlers = new HashMap<BlurHandler, HandlerRegistration>();

    public VRichTextArea() {
        createRTAComponents();
        initWidget(fp);
        setStyleName(CLASSNAME);
        fp.add(rta);
        TouchScrollDelegate.enableTouchScrolling(html, html.getElement());
    }

    private void createRTAComponents() {
        if (formatter != null)
            formatter.detachVRTA(this);
        rta = new RichTextArea();
        rta.addInitializeHandler(new InitializeHandler() {
            @Override
            public void onInitialize(InitializeEvent ie) {

                initialised = true;
                IFrameElement fe = IFrameElement.as(rta.getElement());
                final Document cd = fe.getContentDocument();
                if (cd == null)
                    return;
                else {
                    NodeList<com.google.gwt.dom.client.Element> heads = cd
                            .getElementsByTagName("head");
                    HeadElement head = null;
                    if (heads != null && heads.getLength() > 0)
                        head = heads.getItem(0).cast();

                    if (head == null) {
                        head = cd.getDocumentElement().getFirstChildElement()
                                .cast();

                        if (head == null) {
                            cd.insertFirst(head = cd.createElement("head")
                                    .cast());
                        }
                    }
                    LinkElement linkElement = cd.createLinkElement();
                    linkElement.setRel("stylesheet");
                    linkElement
                            .setHref("./VAADIN/addons/richtexttoolbar/styles.css");
                    linkElement.setType("text/css");
                    final HeadElement fHhead = head;
                    fHhead.appendChild(linkElement);
                }

                if (autoGrowHeight) {
                    setAutoHeight();
                }
                if (autoGrowWidth) {
                    setAutoWidth();
                }
                rta.addKeyDownHandler(VRichTextArea.this);

                if (formatter != null)
                    formatter.attachVRTA(VRichTextArea.this);
                // Add blur handlers
                for (Entry<BlurHandler, HandlerRegistration> handler : blurHandlers
                        .entrySet()) {

                    // Remove old registration
                    handler.getValue().removeHandler();

                    // Add blur handlers
                    addBlurHandler(handler.getKey());
                }

            }
        });
        rta.setWidth("100%");
    }

    public void setEnabled(boolean enabled) {
        if (this.enabled != enabled) {
            // rta.setEnabled(enabled);
            swapEditableArea();
            this.enabled = enabled;
        }
    }

    /**
     * Swaps html to rta and visa versa.
     */
    private void swapEditableArea() {
        String value = getValue();
        if (html.isAttached()) {
            fp.remove(html);
            if (BrowserInfo.get().isWebkit()) {
                if (internalFormatter)
                    fp.remove(formatter);
                createRTAComponents(); // recreate new RTA to bypass #5379
                if (internalFormatter)
                    fp.add(formatter);
            }
            fp.add(rta);
        } else {
            fp.remove(rta);
            fp.add(html);
        }
        setValue(value);
    }

    /** For internal use only. May be removed or replaced in the future. */
    public void selectAll() {
        /*
         * There is a timing issue if trying to select all immediately on first
         * render. Simple deferred command is not enough. Using Timer with
         * moderated timeout. If this appears to fail on many (most likely slow)
         * environments, consider increasing the timeout.
         * 
         * FF seems to require the most time to stabilize its RTA. On Vaadin
         * tiergarden test machines, 200ms was not enough always (about 50%
         * success rate) - 300 ms was 100% successful. This however was not
         * enough on a sluggish old non-virtualized XP test machine. A bullet
         * proof solution would be nice, GWT 2.1 might however solve these. At
         * least setFocus has a workaround for this kind of issue.
         */
        new Timer() {
            @Override
            public void run() {
                rta.getFormatter().selectAll();
            }
        }.schedule(320);
    }

    public void setReadOnly(boolean b) {
        if (isReadOnly() != b) {
            if (!b)
                lineToHeightFactor = 0;
            swapEditableArea();
            readOnly = b;
        }
        // reset visibility in case enabled state changed and the formatter was
        // recreated
        if (internalFormatter)
            formatter.setVisible(!readOnly);
    }

    private boolean isReadOnly() {
        return readOnly;
    }

    @Override
    public void setHeight(String height) {
        super.setHeight(height);

        if (height == null || height.equals("")) {
            rta.setHeight("");
        }
    }

    @Override
    public void setWidth(String width) {
        if (width.equals("")) {
            /*
             * IE cannot calculate the width of the 100% iframe correctly if
             * there is no width specified for the parent. In this case we would
             * use the toolbar but IE cannot calculate the width of that one
             * correctly either in all cases. So we end up using a default width
             * for a RichTextArea with no width definition in all browsers (for
             * compatibility).
             */

            super.setWidth(toolbarNaturalWidth + "px");
        } else {
            super.setWidth(width);
        }
    }

    @Override
    public void onKeyPress(KeyPressEvent event) {
        if (maxLength >= 0) {
            Scheduler.get().scheduleDeferred(new Command() {
                @Override
                public void execute() {
                    if (rta.getHTML().length() > maxLength) {
                        rta.setHTML(rta.getHTML().substring(0, maxLength));
                    }
                }
            });
        }
        if (autoGrowWidth) {
            Scheduler.get().scheduleDeferred(new Command() {
                @Override
                public void execute() {
                    setAutoWidth();
                }
            });
        }
        if (autoGrowHeight) {
            Scheduler.get().scheduleDeferred(new Command() {
                @Override
                public void execute() {
                    setAutoHeight();
                }
            });
        }
    }

    @Override
    public void onKeyDown(KeyDownEvent event) {
        // delegate to closest shortcut action handler
        // throw event from the iframe forward to the shortcuthandler
        ShortcutActionHandler shortcutHandler = getShortcutHandlerOwner()
                .getShortcutActionHandler();
        if (shortcutHandler != null) {
            shortcutHandler
                    .handleKeyboardEvent(com.google.gwt.user.client.Event
                            .as(event.getNativeEvent()),
                            ConnectorMap.get(client).getConnector(this));
        }
    }

    private ShortcutActionHandlerOwner getShortcutHandlerOwner() {
        if (hasShortcutActionHandler == null) {
            Widget parent = getParent();
            while (parent != null) {
                if (parent instanceof ShortcutActionHandlerOwner) {
                    break;
                }
                parent = parent.getParent();
            }
            hasShortcutActionHandler = (ShortcutActionHandlerOwner) parent;
        }
        return hasShortcutActionHandler;
    }

    @Override
    public int getTabIndex() {
        return rta.getTabIndex();
    }

    @Override
    public void setAccessKey(char key) {
        rta.setAccessKey(key);
    }

    @Override
    public void setFocus(final boolean focused) {
        /*
         * Similar issue as with selectAll. Focusing must happen before possible
         * selectall, so keep the timeout here lower.
         */
        new Timer() {

            @Override
            public void run() {
                BrowserInfo bi = BrowserInfo.get();
                if (focused && (bi.isChrome() || bi.isSafari()))
                    rta.getElement().focus();
                rta.setFocus(focused);
            }
        }.schedule(300);
    }

    @Override
    public void setTabIndex(int index) {
        rta.setTabIndex(index);
    }

    /**
     * Set the value of the text area
     * 
     * @param value
     *            The text value. Can be html.
     */
    public void setValue(String value) {
        if (rta.isAttached()) {
            rta.setHTML(value);
            if (initialised) {
                if (autoGrowWidth) {
                    Scheduler.get().scheduleDeferred(new Command() {
                        @Override
                        public void execute() {
                            setAutoWidth();
                        }
                    });
                }
                if (autoGrowHeight) {
                    Scheduler.get().scheduleDeferred(new Command() {
                        @Override
                        public void execute() {
                            setAutoHeight();
                        }
                    });
                }
            }
        } else {
            html.setHTML(value);
        }
    }

    /**
     * Get the value the text area
     */
    public String getValue() {
        if (isAttached() && rta.isAttached()) {
            return rta.getHTML();
        } else {
            return html.getHTML();
        }
    }

    /**
     * Browsers differ in what they return as the content of a visually empty
     * rich text area. This method is used to normalize these to an empty
     * string. See #8004.
     * 
     * @return cleaned html string
     */
    public String getSanitizedValue() {
        BrowserInfo browser = BrowserInfo.get();
        String result = getValue();
        if (browser.isFirefox()) {
            if ("<br>".equals(result)) {
                result = "";
            }
            if (!result.isEmpty() && result.endsWith("<br>")
                    && !result.endsWith("<br><br>")) {
                result = result.substring(0, result.length() - 4);
            }
        } else if (browser.isWebkit()) {
            if ("<br>".equals(result) || "<div><br></div>".equals(result)) {
                result = "";
            }
        } else if (browser.isIE()) {
            if ("<P>&nbsp;</P>".equals(result)) {
                result = "";
            }
        } else if (browser.isOpera()) {
            if ("<br>".equals(result) || "<p><br></p>".equals(result)) {
                result = "";
            }
        }
        return result;
    }

    /**
     * Adds a blur handler to the component.
     * 
     * @param blurHandler
     *            the blur handler to add
     */
    public void addBlurHandler(BlurHandler blurHandler) {
        blurHandlers.put(blurHandler, rta.addBlurHandler(blurHandler));
    }

    /**
     * Removes a blur handler.
     * 
     * @param blurHandler
     *            the handler to remove
     */
    public void removeBlurHandler(BlurHandler blurHandler) {
        HandlerRegistration registration = blurHandlers.remove(blurHandler);
        if (registration != null) {
            registration.removeHandler();
        }
    }

    public Formatter getRTAFormatter() {
        return rta.getFormatter();
    }

    public RichTextArea getRTA() {
        return rta;
    }

    public void setFormatter(VRichTextToolbar formatter) {
        if (formatter == null)
            return;
        if (internalFormatter)
            setInternalFormatter(false);
        else
            formatter.detachVRTA(this);

        this.formatter = formatter;
        this.formatter.attachVRTA(this);
    }

    public void setInternalFormatter(boolean internalFormatter) {
        if (this.internalFormatter == internalFormatter)
            return;

        if (this.internalFormatter && formatter != null) {
            fp.remove(formatter);
        }
        if (formatter != null)
            formatter.detachVRTA(this);

        this.internalFormatter = internalFormatter;

        if (!this.internalFormatter)
            rta.setHeight("");
        else {
            formatter = new VRichTextToolbar();
            fp.remove(rta);
            fp.add(formatter);
            fp.add(rta);
            formatter.attachVRTA(this);
        }
    }

    // TODO find better way than the ugly hack with catch and forget.
    @Override
    protected void onUnload() {
        if (!readOnly) {
            if (internalFormatter)
                fp.remove(formatter);
            try {
                fp.remove(rta);
            } catch (Exception e) {}
        }
        if (formatter != null)
            formatter.detachVRTA(this);
        super.onUnload();
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        if (formatter != null)
            formatter.attachVRTA(this);
        if (!readOnly) {
            // rta.setHTML(getValue());
            if (internalFormatter)
                fp.add(formatter);
            fp.add(rta);
        }
        if (client != null)
            client.sendPendingVariableChanges();
    }

    private void setAutoWidth() {
        IFrameElement fe = IFrameElement.as(rta.getElement());
        Document contentDocument = fe.getContentDocument();
        int w = contentDocument.getBody().getOffsetWidth();

        if (internalFormatter && w < formatter.getOffsetWidth())
            w = formatter.getOffsetWidth();
        setWidth(w + "px");
    }

    private void setAutoHeight() {
        IFrameElement fe = IFrameElement.as(rta.getElement());
        Document contentDocument = fe.getContentDocument();
        BodyElement body = contentDocument.getBody();
        com.google.gwt.dom.client.Element docElem = contentDocument
                .getDocumentElement();
        final int lineCount = countLineBraks();
        if (lineCount == lastNewLineCount) {
            final int sH = body.getScrollHeight();
            if (sH == lastAutoHeight)
                return;
            else {
                // probably formatting changed and so the body height changed
                // or line was to long and has automatic break
                // if size got smaller doing nothing is not good on all browsers
                // but i can prevent size jumps if not setting to small
                // int minH = lineCount * lineToHeightFactor;
                // rta.setHeight(minH + "px");
            }
        } else if (lineCount < lastNewLineCount) {
            int minH = lineCount * lineToHeightFactor;
            rta.setHeight(minH + "px");
        }
        lastNewLineCount = lineCount;
        final int sH = body.getScrollHeight();
        lineToHeightFactor = sH / lineCount;
        // add 4 pixels to be on a safer side with different browsers
        int h = sH + 4;
        lastAutoHeight = h;
        rta.setHeight(h + "px");
    }

    private int countLineBraks() {
        String value = getValue();
        if (value == null)
            return 1;
        BrowserInfo browser = BrowserInfo.get();
        value = value.toLowerCase();
        if (browser.isFirefox())
            value = value.replaceAll("<br>", "\n");
        else if (browser.isWebkit())
            value = value.replaceAll("</div>", "</div>\n");
        if (browser.isIE() || browser.isOpera())
            value = value.replaceAll("</p>", "</p>\n");
        int count = 1;
        for (byte b : value.getBytes()) {
            if (b == '\n')
                count++;
        }
        return count;

    }
}
