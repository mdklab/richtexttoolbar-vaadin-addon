/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.google.gwt.user.client.ui.impl;

/**
 * IE6-specific implementation of rich-text editing.
 */
public class RichTextAreaImplIE10 extends RichTextAreaImplIE6Ex {

    @Override
    public native void initElement() /*-{
                                     var _this = this;
                                     _this.@com.google.gwt.user.client.ui.impl.RichTextAreaImplStandard::onElementInitializing()();
                                     var elem = _this.@com.google.gwt.user.client.ui.impl.RichTextAreaImpl::elem;
                                     var doc = elem.contentWindow.document;
                                     doc.write('<html><head></head><body></body></html>');
                                     setTimeout($entry(function() {
                                     if(_this.@com.google.gwt.user.client.ui.impl.RichTextAreaImplStandard::initializing == false) {
                                         return;
                                     }
                                     
                                     // Attempt to set the iframe document's body to 'contentEditable' mode.
                                     // There's no way to know when the body will actually be available, so
                                     // keep trying every so often until it is.
                                     // Note: The body seems to be missing only rarely, so please don't remove
                                     // this retry loop just because it's hard to reproduce.
                                     var elem =_this.@com.google.gwt.user.client.ui.impl.RichTextAreaImpl::elem;
                                     var doc = elem.contentWindow.document;
                                     if (!doc.body) {
                                     // Retry in 50 ms. Faster would run the risk of pegging the CPU. Slower
                                     // would increase the probability of a user-visible delay.
                                     setTimeout(arguments.callee, 50);
                                     return;
                                     }
                                     
                                     doc.body.contentEditable = true;
                                     
                                     // Send notification that the iframe has reached design mode.
                                     _this.@com.google.gwt.user.client.ui.impl.RichTextAreaImplStandard::onElementInitialized()();
                                     }, 1));
                                     }-*/;
    //

}
