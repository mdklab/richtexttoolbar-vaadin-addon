# Richtexttoolabr Add-on for Vaadin 7.3

This Add-on is intended as a drop-in replacement for the RichTextArea by Vaadin. Most of it is just a copy of the original.

Added functionality so far:

 - server side widget for the toolbar, which allows to place it decoupled from the text area. It is possible to attach multiple text areas to one toolbar.
 - set the height to grow and shrink with the text
 - i18n through sass variable
 - more styling of toolbar through css
 - styling content of area with css


## Styling

The RichTextArea will use ./VAADIN/addons/richtexttoolbar/styles.css
Just overwrite this in your application to change the styling of the Area.

## I18n

Define $rtt-icons in your theme and then use @include richtexttoolbar;
For example a german translation:

    $rtt-icons:   "b" "\f032" "Fett",
        "i" "\f033" "Kursiv",
        "u" "\f0cd" "Unterstrichen",
        "sub" "\f12c" "Hochgestellt",
        "super" "\f12b" "Tiefgestellt",
        "left" "\f036" "Links",
        "center" "\f037" "Zentriert",
        "right" "\f038" "Rechts",
        "strike" "\f0cc" "Durchgestrichen",
        "indent" "\f03c" "Einrücken",
        "outdent" "\f03b" "Ausrücken",
        "hr" "\2014" "Horizontale Linie einfügen",
        "ol" "\f0cb" "Nummerierte Liste einfügen",
        "ul" "\f0ca" "Liste einfügen",
        "img" "\f03e" "Bild einfügen",
        "link" "\f0c1" "Link erstellen",
        "unlink" "\f127" "Link entfernen",
        "clear" "\f12d"  "Formattierung entfernen";
        
    .mytheme {
        @include richtexttoolbar;
    }

# Download release

Official releases of this add-on is available at Vaadin Directory. For Maven instructions, download and reviews, go to http://vaadin.com/addon/richtexttoolbar

## Contributions

Contributions are welcome, but there are no guarantees that they are accepted as such. Process for contributing is the following:
- Fork this project
- Create an issue to this project about the contribution (bug or feature) if there is no such issue about it already. Try to keep the scope minimal.
- Develop and test the fix or functionality carefully. Only include minimum amount of code needed to fix the issue.
- Refer to the fixed issue in commit
- Send a pull request for the original project
- Comment on the original issue that you have implemented a fix for it

## License & Author

Add-on is distributed under Apache License 2.0. For license terms, see LICENSE.txt.

Richtexttoolabr is written by Raffael Bachmann (a4D Architekten AG)